<table color="FFFFFF">
    <thead>
        <tr>
            <th>Emmanuel Courtoux au Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/emmanuel-courtoux_20200403_155549.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Martin ... au Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/martin_20200403_174017.jpg
" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

##### Les volontaires qui ont participé à cette initiative de fabrication sont:

---

###### Pour Fablab Digiscope:
 + `Romain Di Vozzo`
 + `Emmanuel Courtoux`
 + `Nawel Khenak`
 + `Sylvie Delaët`
 + `Nicolas Thiéry`

---

###### Pour Centrale Supélec:
+ `Érika Jean-Bart`
+ `Yannick ...`

---

###### Pour Polytech:
+ `Mickaël Quentin`
+ `Cédric Koeninger`

---

###### Pour FAST:
+ `Lionel Auffray`

---

###### Avec le soutien inconditionnel de:
+ `Sylvie Retailleau et Stéphane retailleau`
+ `Michel Beaudouin Lafon`
+ `Johanne Cohen`
+ `François Grossin`
+ `Martin ...`
+ `Tidjane ...`

---
