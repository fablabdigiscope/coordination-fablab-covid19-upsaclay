##### Chat de Coordination sur l'Application Mobile Telegram

Pour coordonner l'effort collectif de fabrication de visières de protection pour les personnels soignants des hôpitaux du périmétre saclaysien, nous avons ouvert un canal sur Telegram. Merci de nous écrire à **romain.di-vozzo@universite-paris-saclay.fr** en renseignant obligatoirement les informations suivantes:

---

| 1 | Titre de votre email: |
- | :-
| # | Participation au Canal Telegram de l'Initiative de Fabrication Covid19 Saclay |

---

| 2 | Nom de l'institution, de la personne ou du groupe de personnes: |
- | :-
| # | Ce peut-être un fablab, un particulier, une entreprise. Tous es acteurs sont invités à proposer leurs besoins ou leur participation de fabrication |

---

| 3 | Numéro de téléphone |
- | :-
| # | +33 6... |
