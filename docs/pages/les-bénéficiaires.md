##### Les visières fabriquées par les membres de l'Université Paris-Saclay ont été offertes aux établiseements suivants:

###### Hôpital d'Orsay
###### Hôpital du Kremlin-Bicêtre
###### Hôpital de Garches
###### Hôpital de Bligny
###### Hôpital de Percy
###### Hôpital d'Arpajon
###### Hôpital Necker
###### Institut Gustave Roussy
