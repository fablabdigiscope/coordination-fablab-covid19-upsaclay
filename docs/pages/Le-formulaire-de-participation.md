##### Formulaire de participation à l'initiative de fabrication du territoire saclaysien

Pour contribuer à l'effort collectif de fabrication de visières de protection pour les personnels soignants des hôpitaux du périmétre saclaysien, merci de nous écrire à **romain.di-vozzo@universite-paris-saclay.fr** en renseignant obligatoirement les informations suivantes:

---

| 1 | Titre de votre email: |
- | :-
| # | Participation à l'Initiative de Fabrication Covid19 Saclay |

---

| 2 | Nom de l'institution, de la personne ou du groupe de personnes: |
- | :-
| # | Ce peut-être un fablab, un particulier, une entreprise. Tous es acteurs sont invités à proposer leurs besoins ou leur participation de fabrication |

---

| 3 | Le positionnement du/des participants dans le contexte de fabrication pour contrer le COVID19: |
- | :-
| # | Fabriquant de visières ? Avec quelle(s) machine(s) ? |
| # | Utilisateur-Personnel Soignant ? Dans quel établissement ? |
| # | Donateur de matières premières (PLA, Plaque de PETG, Élastiques, etc) ? |
| # | Philanthrope ? |


---

| 4 | Le type d'objet attendus ou proposés: |
- | :-
| # | Pour le moment, nous ne proposons que des visières/shields. Quel design imprimez-vous ? (Merci d'ajouter une photo) |

---

| 5 | Capacité de fabrication de shields ou le besoin de shields: |
- | :-
| # | En unité de shields/jour |

---

| 6 | La localisation géographique du participant(e.es): |
- | :-
| # | L'endroit où vous fabriquez vos shields |
| # | Le Centre Hospitalier où vous travaillez |

---

| 7 | L'adresse email du/des participant(e.es): |
- | :-
| # | Pour le moment, nous ne proposons que des visières/shields |

---

| 8 | Le numéro de téléphone du/des participant(e.es): |
- | :-
| # | Pour vous contacter par sms ou vous permettre de rejoindre notre groupe Telegram |

---
