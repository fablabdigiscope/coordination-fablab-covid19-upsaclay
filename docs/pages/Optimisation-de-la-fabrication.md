###### Optimisation des séquences d'impression par Emmanuel Courtoux

---

A partir du moment ou nous avons commencé à imprimer les serres-têtes par piles (donc les uns sur les autres), Emmanuel a consigné ici l'ensemble des informations qui permettent d'anticiper et de contrôler le rapport entre le type de machine, le temps d'impression de chaque fichier et le nombre de serres-têtes qu'il est possible d'imprimer en une seule impression.

Le poids de chacun des designs et la longueur du filament de PLA utilisés pour chaque impression sont également exprimés.

Ont été pris en considération deux design de serres-têtes distinct, un épais - Prusa 3 - et un plus fin.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Optimisation des séquences d'impression 3D par Emmanuel Courtoux</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><iframe src="https://fablabdigiscope.gitlab.io/coordination-fabrication-covid19-saclay/images/fichiers/manu-data-print-shields-1.pdf" height="400" width="600"></iframe></td>
        </tr>
    </tbody>
    <thead>
</table>
manu-data-print-shields-1.pdf

---
