<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br /><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

---

<p>Nous avons commencé par imprimer les serres-têtes de Prusa sur ULTIMAKER S5</font></p>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Visière Prusa 2</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fabrication-covid19-saclay/images/prusa-shield-2.medium.png" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Paramètres d'impression des Shields Prusa 2 et 3</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-0.png" /></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>Visière de Prusa dans Cura (logiciel pour impression 3D)</th>
        </tr>
    </thead>
</table>


---


<table color="FFFFFF">
    <thead>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-1.jpg" /></td>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-2.jpg" /></td>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-3.jpg" /></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>PLA / Buse 0.8mm / 30% de remplissage</th>
            <th>x2 Serres-têtes par impression</th>
            <th>2h40 d'impression</th>
        </tr>
    </thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>First Print Prusa Shield 3</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-4.png" /></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>Software Slicing</th>
        </tr>
    </thead>
</table>



---


<table color="FFFFFF">
    <thead>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-5.jpg" /></td>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-6.jpg" /></td>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/prints-covid19-7.jpg" /></td>
        </tr>
    <thead>
        <tr>
        <th>PLA / Buse 0.8mm / 30% de remplissage</th>
        <th>x2 Serres-têtes par impression</th>
        <th>2h20 d'impression</th>
        </tr>
    </thead>
</table>

---



<p>Dans la mesure du possible nous préfèrerons la découpe de visières au laser plutôt que l'impression 3D de serres-têtes de visières.</p>

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Shield Facial à découper au laser</th>
        </tr>
    </thead>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/covid-laser-cut-shield-1.png" /></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>https://hackaday.io/project/170481-laser-cut-medical-shield</th>
        </tr>
    </thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Shield Facial à découper au laser</th>
        </tr>
    </thead>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/covid-laser-cut-shield-2.png" /></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>Contacts:konrad.klepacki(at)wp.pl / Mateusz Dyrda mateusz.dyrda(at)srskalp.pl
</font></th>
        </tr>
    </thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Shield Facial à découper au laser</th>
        </tr>
    </thead>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/covid-laser-cut-shield-3.png" /></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>Feel free to use it under CC BY-NC 4.0 license</th>
</font></th>
        </tr>
    </thead>
</table>

---

<p>Simulation du positionnement de découpe-laser de la visière en PETG</p>

<table color="FFFFFF">
    <thead>
        <tr>
            <td rowspan=1><img src="https://gitlab.com/fablabdigiscope/fablabdigiscope.gitlab.io/raw/master/themes/beautifulhugo/static/img/covid-laser-cut-possible-sheet.png" /></td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>Nous pourrions découper 5-6 shields en env 10min sur notre Epilog M2+</th>
        </tr>
    </thead>
</table>
