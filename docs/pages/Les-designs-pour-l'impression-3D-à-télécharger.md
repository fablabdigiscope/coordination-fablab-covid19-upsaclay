
###### Le 12 avril 2020,

###### Vous pouvez trouver les fichiers 3D de visières empilées sur la plateforme Youmagine:

<iframe src="https://www.youmagine.com/designs/covid19-stacked-headsets-for-um3-um3e-us5/embed" width="735" height="219" frameborder="0"></iframe>

###### Y sont disponibles:
###### UMS5_UM5S_LARGE_45_STACK.3mf
###### UMS5_UM5S_LARGE_36_STACK.3mf
###### UMS5_UM5S_LARGE_30_STACK.3mf
###### UMS5_UM5S_LARGE_15_STACK.3mf
###### UM3E_LARGE_8_STACK.3mf
###### UM3_LARGE_1.3mf


###### UM3_THIN_30_STACK.stl
###### UM3_THIN_25_STACK.stl
###### UM3_THIN_20_STACK.stl
###### UM3_THIN_15_STACK.stl
###### UM3_THIN_10_STACK.stl
###### UM3_THIN_5_STACK.stl
###### UM3_THIN_1.stl
