##### Compression Milling of Face Shields

###### This experiment happened at Fablab Digiscope | Université Paris-Saclay, in remote collaboration with [Zach Fredin](https://zfredin.pages.cba.mit.edu/home/), lead locally by Romain Di Vozzo and Emmanuel Courtoux. The approach was to adapt/replicate the experiment led by Zach.
###### The design of the Face Shield was provided by the CBA/MIT. We started this experiment with a new Shapeoko3 XXL we used for the first time.

###### We used Carbide Create to make the toolpath

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/2.PNG" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### We used Carbide Motion to control the router's movements

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/3.PNG" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### We first had to level up the waste-board by 40mm to avoid the end effector to go off the rails
###### To do so we made a custom modular waste-board out of 2 plywood sheets of 20mm (thickness) x 460mm x 610mm each
###### On top of the wood waste-board we stuck a sheet of 10mm HDPE (the green one on the pictures) with a strong doubled-sided tape from Tesa. The sheet was 510mm x 502mm we had it in stock at this size). It was the bottom of the compression-milling box.
###### Then we milled a 4mm deep x 490mm x 490mm square inside this 10mm thick HDPE waste-board/compression-milling box bottom with a 1/2" surfacing bit. At the same time the bottom-plan of the HDPE was levelled properly.
###### In parallel we cut PETG sheets of 490mm 490mm out of bigger PETG sheets (0.88mm x 500mm x 1000mm) with a manual cutter. The PETG sheets we had ordered weren't supposed to be used on the this machine so we had to resize them manually one-by-one.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/15.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### We piled up 5 sheets of PETG of .88mm x 490mm x 490mm inside the 4mm-deep square we cut into the HDPE waste-board/compression-milling box bottom.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/16.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### On top of the PETG sheets, we placed a second green sheet of HDPE as a top/cover to apply the pressure on top of the PETG pile underneath it.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/19.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### We decided to place 4 screws to close the "sandwich" of HDPE+PETG+HDPE (bottom-middle-top).
###### According to the Face shield Design's size, we chose to drill the holes for the screws at 100mm away from any border of the HDPE sheet. We made sure the milling-bit will not meet them on its path. As Carbide Create doesn't allow for drilling toolpath, we decided to drill the holes manually. We used a PostIt as a guide to place the holes. We drilled with a Dremel through bothe the HDPE and the PETG.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/17.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/18.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### We made sure the screws wouldn't hit the wood waste-board and would stay stuck inside the HDPE. In our case, the screws had to be a little longer than 20mm.
###### Then we had to clean the face shield file to removed double lines. We did it in CorelDraw2019.
###### We also had to turn it into a single line as it came as a bunch of separated lines. We did "combine", "boundary" and "merge" in Corel Draw and exported the file as an .svg
###### We uploaded the .svg to Carbide Create to make the toolpath, with 10mm-wide tabs on it.
###### We divided the task in 2 parts: We first milled the HDPE cover with a 1/8" 2 flutes bit from Carbi-Universal as an "outline", and restarted the job to mill the PETG sheets trapped between the HDPE cover and waste-board/compression-milling box bottom with the same 1/8" 2 flutes bit.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/20.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/21.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/22.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/23.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/24.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### We unscrewed the top/cover to remove the PETG.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/25.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/26.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/27.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---


<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/28.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/29.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/30.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/31.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### This is how the shields came out

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/32.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/33.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### More comments (to come)
