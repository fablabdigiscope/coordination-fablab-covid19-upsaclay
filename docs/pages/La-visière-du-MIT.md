###### Des membres du Center for Bits and Atoms ont participé au développement de cette visières dans le cadre du Projet Magnus, puis l'ont forké pour faire le FabShield.

---

###### La page maintenue par [Zach Fredin](https://zfredin.pages.cba.mit.edu/home/) sur le gitlab du CBA pour un fraisage sous compression de feuilles de PETG est [là](https://gitlab.cba.mit.edu/alfonso/fabshield), ainsi que les fichiers correspondants. Dans l'urgence de fabrication actuelle, nous repoussons temporairement les tests de fraisage au profit de la découpe laser.

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234700.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><iframe src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/fichiers/romain-shield-mit.pdf" height="400" width="600"></iframe></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>Manuel d'assemblage de la visière du Projet Magnus</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><iframe src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/fichiers/face-shield-instructions_v1.3b.pdf" height="400" width="600"></iframe></td>
        </tr>
    </tbody>
    <thead>
</table>

---

###### Cette visière a été découpée au laser dans une feuille de PETG de 1mm. La rigidité est très bonne, mais la pliure manuelle ne pourra pas être faite par tout le monde.

---

###### Dans les conditions d'exercice des soins et de la médecine liées à la pandémie du Covid19, les personnels hospitaliers sont forcés d'utiliser plusieurs fois des consommables jetables.

---

###### Cette visière pourrait permettre une re-utilisation plus sécurisée, à la fois par design, et par les propriétés du matériau PETG.

---

###### Il s'agit d'un design de kirigami: un design qui inclue des coupes (traversant donc le matériau de part en part) et des fausses coupes (qui entament le matériau dans son épaisseur sans le traverser) afin d'assouplir le matériau et de le rendre pliable par endroits, par "design".

---

###### Dans le modèle ci-dessus, les fausses-coupes son inexistantes. J'ai simplement découpé les contours puis ai plié ce masque à la main avec une règle.

---

###### Une feuille de PETG de 0.75mm serait plus facile à plier. Le masque serait moins rigide mais le serait suffisamment à mon sens.

---

###### Il reste que le temps de découpe peut sembler un peu long

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/mit-shield_20200408_203959.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_204049.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_232839.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_232847.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_232854.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234854.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234907.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234911.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234916.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234921.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234929.jpg" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234944.jpg
" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---

<table color="FFFFFF">
    <thead>
        <tr>
            <th>CBA/MIT Shield cut at Fablab Digiscope</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1><img src="https://fablabdigiscope.gitlab.io/coordination-fablab-covid19-upsaclay/images/mit-shield_20200408_234949.jpg
" /></td>
        </tr>
    </tbody>
    <thead>
</table>

---
