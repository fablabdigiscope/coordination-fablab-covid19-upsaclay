**Université Paris-Saclay organise la mobilisation de ses ressources et de ses équipements pour participer à l'action de soutien aux hôpitaux et aux personnels-soignants de son périmètre local.**

---

![](images/index/logo-upsaclay.png)

---

Une équipe est à l'oeuvre pour coordonner cet effort collectif afin d'organiser la fabrication et la dissémination de visières de protection pour les personnels soignants à partir des nombreuses imprimantes 3D et découpeuses Laser présentes sur le réseau distribué de machines du campus universitaire.

---

![](images/index/fablabdigiscope_logo.png)

---

Le Fablab Digiscope | Universite Paris-Saclay et les Laboratoires de Fabrication Numérique présents sur le campus contribuent à la mise en place de la filière de fabrication de ces visières

Plusieurs institutions et personnes se joignent à Université Paris-Saclay dans cette initiative:


...
